extends Node2D

onready var smoke_Effect = $SmokeEffect


func _ready():
	_on_HSlider_value_changed(20)
	


func _on_HSlider_value_changed(value):
	smoke_Effect.start($HSlider.value)
