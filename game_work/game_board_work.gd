extends Node2D

export var board_cols : int = 7
export var board_rows : int = 6

var _board = [
	"0","1","2","3","4","5","6","7","8","9",
	"1","2","3","4","5","6","7","8","9","0",
	"2","3","4","5","7","7","8","9","0","1",
	"3","4","5","6","7","8","9","0","1","2",
	"4","5","6","7","8","9","0","1","2","3",
	"5","6","7","8","9","0","1","2","3","4",
	"6","7","8","9","0","1","2","3","4","5",
]

func _ready():
	Logger.trace("[GameBoardWork] _ready")
	$GameBoard.columns = board_cols
	$GameBoard.rows = board_rows
	$GameBoard.create()
	var _count = 0
	
#	for _loc in Game.game_board.pieces:
#		var _piece = Game.game_board.pieces[_loc]
#		print(_piece)
#		_piece.type = _board[_count]
#		_count += 1
		
	print($GameBoard.__dump__("type"))
	$GameBoard.draw()
	Game.connect("piece_clicked", self, "_on_click_piece")
	
	Game.music_manager.play("Stage2")
	
func _on_click_piece(piece):
	Logger.trace("[GameBoardWork] _on_click_piece")
	$GameBoard.draw()
