extends Node2D

export var scene : PackedScene

func _ready():
	_on_Timer_timeout()


func _on_Timer_timeout():
	var _show = scene.instance()
	add_child(_show)
	var _amount = int(rand_range(-10, 10))
	var _pn = ""
	if _amount > 0:
		_pn = "+"
	var message = "%s%s" % [_pn, _amount]
	
	_show.start(message, Color(randf(), randf(), randf()), Vector2(rand_range(0,100), rand_range(0,100)))
