tool
extends Node2D

export var show_zones : bool = true
var draw_font 

func _ready():
	draw_font = load("res://game/fonts/graphic_pixel_10.tres")
	draw_font.size = 10

func _process(delta):
	update()

func _draw():
		
	
	if (show_zones):
		draw_line(Vector2(-50,60), Vector2(1010,60), Color.purple, 2.0, false)
		draw_line(Vector2(-50,80), Vector2(1010,80), Color.green, 2.0, false)
		draw_line(Vector2(-50,500), Vector2(1010,500), Color.green, 2.0, false)
		draw_line(Vector2(160,-50), Vector2(160,590), Color.green, 2.0, false)
		draw_line(Vector2(800,-50), Vector2(800,590), Color.green, 2.0, false)
		draw_string(draw_font, Vector2(162, 14), "UI", Color.white)
		draw_string(draw_font, Vector2(162, 94), "Main", Color.white)
