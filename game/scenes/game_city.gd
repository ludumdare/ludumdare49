extends Node2D

export var show_speed : float = 0.5

var city_blocks = []

onready var waiter = $Waiter
onready var device = $Device
onready var dialog = $Dialog
onready var dimmer = $Dimmer

onready var dialog_text = $Dialog/Baloon/Text
onready var dialog_click = $Dialog/Baloon/Click
onready var dialog_collider = $Dialog/Area2D

onready var waiter_idle = $Waiter/Idle
onready var waiter_sad = $Waiter/Sad
onready var waiter_joy = $Waiter/Joy
onready var waiter_show = $Waiter/Show

var waiter_pos : Vector2
var device_pos : Vector2
var dialog_pos : Vector2

var waiter_hpos : Vector2
var device_hpos : Vector2
var dialog_hpos : Vector2


signal dialog_clicked()


func _init():
	
	pass
	

func _ready():
	
	waiter.visible = true
	dialog.visible = true
	device.visible = true
	dimmer.visible = true
	
	waiter_pos = waiter.position
	device_pos = device.position
	dialog_pos = dialog.position
	
	waiter_hpos = waiter_pos + Vector2.LEFT * 400
	dialog_hpos = dialog_pos + Vector2.LEFT * 400
	device_hpos = device_pos + Vector2.RIGHT * 600
	
	Game.connect("city_block_clicked", self, "_on_city_block_clicked")
	
	_hide(0)
	dialog_click.visible = false

	city_blocks = $CityBlocks.get_children()
	
	var _i = 0
	
	for _block in city_blocks:
		var _game_block = Game.game_blocks[_i]
		_block.configure(_game_block.icon, _game_block.id)
		if (_game_block.is_completed):
			_block.make_complete()
		if (_game_block.is_alerted):
			_block.make_alert()
		
		_i += 1
	
	if (Game.show_tutorial):
		Game.show_tutorial = false
		dialog_click.visible = true
		_run_tutorial()
		return
		
	_show_waiter("idle")
	
	if Game.game_round > 4:
		_alert_block()
		
	if Game.game_round > 10:
		_alert_block()
		
	if Game.game_round > 20:
		_alert_block()
		
	_alert_block()
	
	Game.game_round += 1
	
		
	Game.timer.paused = false
		

func start():
	_hide()
	
	
func show():
	_show()
	
	
func hide():
	_hide()
	
	
func _hide(speed = show_speed, hide_waiter = true, hide_dialog = true, hide_device = true):

	if (hide_waiter):	
		var _t1 = Tween.new()
		add_child(_t1)
		_t1.interpolate_property(waiter, "position", waiter.position, waiter_hpos, speed, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
		_t1.start()
	
	if (hide_dialog):	
		var _t2 = Tween.new()
		add_child(_t2)
		_t2.interpolate_property(dialog, "position", dialog.position, dialog_hpos, speed, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
		_t2.start()
	
	if (hide_device):	
		var _t3 = Tween.new()
		add_child(_t3)
		_t3.interpolate_property(device, "position", device.position, device_hpos, speed, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
		_t3.start()
	
	dimmer.visible = false
	

func _show(speed = show_speed, show_waiter = true, show_dialog = true, show_device = true):
	
	if (show_waiter):
		var _t1 = Tween.new()
		add_child(_t1)
		_t1.interpolate_property(waiter, "position", waiter.position, waiter_pos, speed, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		_t1.start()
	
	if (show_dialog):
		var _t2 = Tween.new()
		add_child(_t2)
		_t2.interpolate_property(dialog, "position", dialog.position, dialog_pos, speed, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		_t2.start()
		
	if (show_device):
		var _t3 = Tween.new()
		add_child(_t3)
		_t3.interpolate_property(device, "position", device.position, device_pos, speed, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		_t3.start()
	
	dimmer.visible = true


func _show_waiter(name):
	
	waiter_idle.visible = false
	waiter_sad.visible = false
	waiter_joy.visible = false
	waiter_show.visible = false
	
	match name.to_lower():
		
		"idle"		: waiter_idle.visible = true
		"sad"		: waiter_sad.visible = true
		"joy"		: waiter_joy.visible = true
		"show"		: waiter_show.visible = true
		

func _alert_block():

	var _block
	
	while (!_block):
		
		var _rand = floor(rand_range(0, Game.game_blocks.size()))
		
		var _game_block = Game.game_blocks[_rand]
		
		if (_game_block.is_open):
			_block = city_blocks[_rand]
			_block.icon = _game_block.icon
			_block.resources = _game_block.resources
			_block.resource_amounts = _game_block.resource_amounts
			
			_block.make_alert()
			_game_block.is_alerted = true
			_game_block.is_open = false

			print(Game.timer.time_left)
			Game.timer.paused = true
			var _time_left = Game.timer.time_left
			var _time = _time_left + 120
			Game.timer.wait_time = _time
			Game.timer.paused = false
			Game.timer.paused = true

func _run_tutorial():
	
	# prepare the device
	_hide(0)
	
	$Device/GameBoard/GoButton.visible = false
	$Device/GameBoard/CancelButton.visible = false
	$Device/GameBoard/Building.visible = false
	$Device/GameBoard/BuildingName.visible = false
	$Device/GameBoard/Resources.visible = false	
	
	yield(get_tree().create_timer(0.5), "timeout")
	
	_show(0.5, true, true, false)
	_show_waiter("idle")
	
	dialog_text.text = "hi! i'm walter the waiter"
	yield(self, "dialog_clicked")
	dialog_click.visible = false

	dialog_text.text = "people say i'm a super hero"
	yield(self, "dialog_clicked")

	dialog_text.text = "but all i really am is a waiter"
	yield(self, "dialog_clicked")

	dialog_text.text = "who found a nifty gadget"
	yield(self, "dialog_clicked")

	dialog_text.text = "called"
	yield(self, "dialog_clicked")

	dialog_text.text = "the table stabler series 7"
	yield(self, "dialog_clicked")

	_show(2.0, false, false, true)
	_show_waiter("show")
	Game.game_board.message_text.visible = true
	dialog_text.text = "here it is"
	yield(self, "dialog_clicked")

	dialog_text.text = "she is a litte unstable"
	yield(self, "dialog_clicked")

	dialog_text.text = "but she has helped me"
	yield(self, "dialog_clicked")

	dialog_text.text = "fix many an unstable table"
	yield(self, "dialog_clicked")

	_show_waiter("sad")
	dialog_text.text = "but, sadly i am getting old"
	yield(self, "dialog_clicked")

	dialog_text.text = "and i want to retire"
	yield(self, "dialog_clicked")

	dialog_text.text = "so i am really glad you have"
	yield(self, "dialog_clicked")

	_show_waiter("joy")
	dialog_text.text = "agreed to takeover for me!"
	yield(self, "dialog_clicked")

	dialog_text.text = "the world needs you"
	yield(self, "dialog_clicked")

	dialog_text.text = "to be the new hero!"
	yield(self, "dialog_clicked")

	_hide(1.0, false, false, true)
	_show_waiter("idle")
	dialog_text.text = "behind me you can see the city"
	yield(self, "dialog_clicked")

	dialog_text.text = "there is an unstable table alert"
	yield(self, "dialog_clicked")

	city_blocks[14].make_alert()
	dialog_text.text = "use your mouse and click the alert"
	yield(Game, "city_block_clicked")

	$Device/GameBoard/Building.visible = true
	$Device/GameBoard/BuildingName.visible = true
	$Device/GameBoard/Resources.visible = true
	
	_show_waiter("joy")
	dialog_text.text = "great!"
	yield(self, "dialog_clicked")

	dialog_text.text = "the TS7 is starting up!"
	yield(self, "dialog_clicked")

	_show(1.0, false, false, true)
	_show_waiter("show")
	dialog_text.text = "the ts7 will show you"
	yield(self, "dialog_clicked")

	dialog_text.text = "what kitchen items you"
	yield(self, "dialog_clicked")

	dialog_text.text = "need to collect to"
	yield(self, "dialog_clicked")

	dialog_text.text = "make the table stable"
	yield(self, "dialog_clicked")

	_show_waiter("idle")
	dialog_text.text = ". . ."
	yield(self, "dialog_clicked")

	dialog_text.text = "you have 5 minutes to"
	yield(self, "dialog_clicked")

	dialog_text.text = "make the table stable"
	yield(self, "dialog_clicked")

	dialog_text.text = " . . ."
	yield(self, "dialog_clicked")

	dialog_text.text = "one last hint"
	yield(self, "dialog_clicked")
	
	dialog_text.text = "stay away from the chilli peppers"
	yield(self, "dialog_clicked")
	
	dialog_text.text = "they will overheat your ts7"
	yield(self, "dialog_clicked")
	
	dialog_text.text = "good luck!"
	yield(self, "dialog_clicked")
	
	dialog_text.text = ":)"
	yield(self, "dialog_clicked")
	
	dialog_text.text = "press go on the ts7"
	yield(self, "dialog_clicked")

	$Device/GameBoard/GoButton.visible = true
	dialog_text.text = "to get started"
	yield(self, "dialog_clicked")



func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.is_pressed():
				emit_signal("dialog_clicked")
				


func _on_city_block_clicked(city_block):
	var _game_block = Game.game_blocks[city_block.id]
	print(_game_block.resources)
	
	Game.current_block = city_block.id
	
	$Device/GameBoard/Building.texture = city_block.sprite.texture
	$Device/GameBoard/Resources/ResourceAmount01.text = "%s" % _game_block.resource_amounts[0]
	$Device/GameBoard/Resources/ResourceAmount02.text = "%s" % _game_block.resource_amounts[1]
	$Device/GameBoard/Resources/ResourceAmount03.text = "%s" % _game_block.resource_amounts[2]
	$Device/GameBoard/Resources/ResourceAmount04.text = "%s" % _game_block.resource_amounts[3]
	
	$Device/GameBoard/Resources/ResourceIcon01.texture = Game.get_icon_texture(_game_block.resources[1])
	$Device/GameBoard/Resources/ResourceIcon02.texture = Game.get_icon_texture(_game_block.resources[2])
	$Device/GameBoard/Resources/ResourceIcon03.texture = Game.get_icon_texture(_game_block.resources[3])
	$Device/GameBoard/Resources/ResourceIcon04.texture = Game.get_icon_texture(_game_block.resources[4])
	
	_show(0.5)
	


func _on_CancelButton_pressed():
	Game.current_block = -1
	_hide(0.5)


func _on_GoButton_pressed():
	Game.timer.paused = false
	SceneManager.transition_to("res://game/scenes/game_match.tscn")
	
