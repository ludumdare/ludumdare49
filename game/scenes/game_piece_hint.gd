extends Node2D

onready var this_sprite = $Frame
onready var scale_tween = $ScaleTween
onready var color_tween = $ColorTween
onready var sprite_icon = $Frame/Icon

func _ready():
	pass
	
	
func Setup(image):
	sprite_icon.texture = image
	slowly_larger() 
	slowly_dimmer()
	
func slowly_larger():
	scale_tween.interpolate_property(this_sprite, "scale", Vector2(0.5, 0.5), Vector2(2.0, 2.0), 1.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	scale_tween.start()
	
func slowly_dimmer():
	color_tween.interpolate_property(this_sprite, "modulate", Color(1,1,1,1), Color(1,1,1,0.1), 1.5, Tween.TRANS_SINE, Tween.EASE_OUT) 
	color_tween.start()
	


func _on_ScaleTween_tween_completed(object, key):
	slowly_larger()


func _on_ColorTween_tween_completed(object, key):
	slowly_dimmer()
