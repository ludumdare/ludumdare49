extends Node2D

signal block_clicked()

onready var collider = $CollisionShape2D
onready var alert = $Status/Alert
onready var complete = $Status/Complete
onready var sprite = $Sprite

var icon : int = 0
var id : int = -1
var resources = []
var resource_amounts = []


var icons = [

	"res://game/assets/city_block_a.png",	
	"res://game/assets/city_block_c.png",	
	"res://game/assets/city_block_d.png",	
	"res://game/assets/city_block_a.png",	
	"res://game/assets/city_block_c.png",	
	"res://game/assets/city_block_d.png",	
	"res://game/assets/city_block_a.png",	
	"res://game/assets/city_block_c.png",	
	"res://game/assets/city_block_d.png",	
	"res://game/assets/city_block_a.png",	
	"res://game/assets/city_block_c.png",	
	"res://game/assets/city_block_d.png",	
	"res://game/assets/city_block_b.png",	
]


func _ready():
	collider.set_deferred("disabled", true)


func configure(icon, id):
	
	self.icon = icon
	self.id = id
	var _texture = load(icons[icon])
	sprite.texture = _texture


func make_alert():
	collider.set_deferred("disabled", false)
	alert.visible = true
	complete.visible = false
	_ping() 


func make_complete():
	alert.visible = false
	complete.visible = true
	_ping() 
	

func _on_CityBlock_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.is_pressed():
				Game.emit_signal("city_block_clicked", self)


func _ping():
	
	var _t = Tween.new()
	add_child(_t)
	_t.interpolate_property($Status, "scale", Vector2(1,1), Vector2(0.5, 0.5), 1.0, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	_t.start()
	yield(_t, "tween_completed")
	_t.queue_free()
	_pong()
	
	
func _pong():
	
	var _t = Tween.new()
	add_child(_t)
	_t.interpolate_property($Status, "scale", Vector2(0.5,0.5), Vector2(1.0, 1.0), 1.0, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	_t.start()
	yield(_t, "tween_completed")
	_t.queue_free()
	_ping()
	
