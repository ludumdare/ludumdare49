extends Sprite

export var glow_color : Color


func _ready():
	_start()
	

func _start():
	
	$Timer.wait_time = rand_range(0.5, 2.25)
	$Timer.start()
	visible = !visible


func _on_Timer_timeout():
	_start()
