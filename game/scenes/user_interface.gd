extends Node2D

onready var score = $"CanvasLayer/#Score"
onready var hiscore = $"CanvasLayer/#HiScore"
onready var time = $"CanvasLayer/#Time"

func _ready():
	Game.connect("resource_collected", self, "_on_resource_collected")
	
	
func _process(delta):
	
	var _min = int(Game.timer.time_left) / 60
	var _sec = int(Game.timer.time_left) % 60
	time.text = "%d:%02d" % [ _min, _sec ]

