extends Node2D

var current_name : String
var current_player : AudioStreamPlayer

func play(name):
	current_name = name
	var current_player = get_node(name) as AudioStreamPlayer
	current_player.play()
	
	
func stop():
	current_player.stop()
	
