
#	Copyright 2018, SpockerDotNet LLC

class_name GameBoard
extends Node2D

export (PackedScene) var hint_piece

onready var smoke_effect = $SmokeEffect

onready var collector = $Board/Collector

onready var collect_bar1 = $Board/Collector/ProgressBar1
onready var collect_bar2 = $Board/Collector/ProgressBar2
onready var collect_bar3 = $Board/Collector/ProgressBar3
onready var collect_bar4 = $Board/Collector/ProgressBar4

onready var collect_icon1 = $Board/Collector/Icon1
onready var collect_icon2 = $Board/Collector/Icon2
onready var collect_icon3 = $Board/Collector/Icon3
onready var collect_icon4 = $Board/Collector/Icon4

onready var collect_label1 = $Board/Collector/Label01
onready var collect_label2 = $Board/Collector/Label02
onready var collect_label3 = $Board/Collector/Label03
onready var collect_label4 = $Board/Collector/Label04

onready var heat_meter = $Board/HeatMeter
onready var heat_icon = $Board/HeatMeter/HeatIcon
onready var heat_bar = $Board/HeatMeter/HeatBar
onready var heat_label = $Board/HeatMeter/HeatLabel

onready var message_text = $Board/Message/Text

var columns = 12
var rows = 8
var pieces = {}
var pieces_copy = {}
var selected : GamePiece
var size = 48
var hint
var block

# the resources that are available on the board
var resources = []
var resource_amounts = []

func shuffle():
	Logger.trace("[GameBoard] shuffle")
	return _shuffle()
	
	
func can_swap(piece):
	Logger.trace("[GameBoard] can_swap")
	if !selected:
		return false
		
	return _can_swap(piece)
	
	
func create():
	Logger.trace("[GameBoard] create")
	
	_create_pieces()
	_resolve_clusters()
	
	while (is_deadlocked()):
		print("is deadlocked")
		for _piece in pieces.values():
			_piece.type = -1
		_resolve_pieces()
		_resolve_clusters()
		
	return

func draw():
	Logger.trace("[GameBoard] draw")
	_draw_pieces()
	$HintTimer.start()
	
	
func get_piece(loc):
	Logger.trace("[GameBoard] get_piece")
	if (pieces.has(loc)):
		return pieces[loc]
	else:
		Logger.warn("No Piece at Location {0}".format(loc))
	

func find_clusters():
	Logger.trace("[GameBoard] find_clusters")
	return _find_clusters()
	
	
func find_shifts():
	Logger.trace("[GameBoard] find_shifts")
	return _find_shifts()
	
	
func remove_clusters(clusters):
	Logger.trace("[GameBoard] remove_clusters")
	return _remove_clusters(clusters)
	
func shift_pieces():
	Logger.trace("[GameBoard] shift_pieces")
	_shift_pieces()
	
	
func resolve_pieces():
	Logger.trace("[GameBoard] resolve_pieces")
	_resolve_pieces()
	
	
func swap(from_piece, to_piece):
	Logger.trace("[GameBoard] swap")
	_swap(from_piece, to_piece)
	
	
func is_deadlocked():
	Logger.trace("[GameBoard] is_deadlocked")
	pieces_copy = {}
	for row in range(rows):
		for col in range(columns):
			var _key = Vector2(col, row)
			var piece = load("res://game/scenes/game_piece.tscn").instance()
#			add_child(piece)
			piece.type = pieces[_key].type
			piece.loc = pieces[_key].loc
			piece.pos = pieces[_key].pos
			pieces_copy[_key] = piece 
	return _find_deadlock(pieces_copy)


func _can_swap(from_piece):
	Logger.trace("[GameBoard] _can_swap")
	var x1 = from_piece.loc.x
	var y1 = from_piece.loc.y
	var x2 = selected.loc.x
	var y2 = selected.loc.y
	
	if ((abs(x1-x2) == 1 && y1 == y2) || (abs(y1 - y2) == 1 && x1 == x2)):
		return true
		
	return false
	
	
func _create_pieces():
	Logger.trace("[GameBoard] _create_pieces")
	
	var root = find_node("Pieces")
	
	randomize()
	
	for y in range(rows):
		for x in range(columns):
#			print("{x}:{y}".format({ "x": x, "y": y}))
#			var x_pos = x * size
#			var y_pos = y * size
			var piece = load("res://game/scenes/game_piece.tscn").instance()
			piece.name = piece.uuid.uuid
			piece.loc = Vector2(x, y)
			piece.pos = piece.loc * size
			piece.position = piece.pos
			piece.type = -1
			pieces[piece.loc] = piece
			root.add_child(piece)
			
			

func find_all_matches():
	var hint_holder = []
	var _clone = pieces.duplicate(false)
	
	for i in range(1, columns):
		
		for j in range(1, rows):
			
			# check for match going up
			var _from = Vector2(i, j)
			var _to = _from + Vector2.UP
			_swap(_clone[_from], _clone[_to], _clone)
			if (_find_clusters(_clone).size() > 0):
				hint_holder.append(_clone[_from])
			_swap(_clone[_from], _clone[_to], _clone)
			
			# check for match going left
			_from = Vector2(i, j)
			_to = _from + Vector2.LEFT
			_swap(_clone[_from], _clone[_to], _clone)
			if (_find_clusters(_clone).size() > 0):
				hint_holder.append(_clone[_from])
			_swap(_clone[_from], _clone[_to], _clone)
			
	return hint_holder
				

func _find_deadlock(game_pieces):
	Logger.trace("[GameBoard] _find_deadlock")
	# check if we match going up or left
	var _deadlock = true;
	for row in range(1, rows):
		for col in range(1, columns):
			# check going up
			var _from_loc = Vector2(col, row)
			var _to_loc = _from_loc + Vector2.UP
			_swap(game_pieces[_from_loc], game_pieces[_to_loc], game_pieces)
			if (_find_clusters(game_pieces).size() > 0):
				_deadlock = false
			_swap(game_pieces[_from_loc], game_pieces[_to_loc], game_pieces)
			# check going left
			_from_loc = Vector2(col, row)
			_to_loc = _from_loc + Vector2.LEFT
			_swap(game_pieces[_from_loc], game_pieces[_to_loc], game_pieces)
			if (_find_clusters(game_pieces).size() > 0):
				_deadlock = false
			_swap(game_pieces[_from_loc], game_pieces[_to_loc], game_pieces)
			
	return _deadlock
	

func _draw_pieces():
	Logger.trace("[GameBoard] _draw_pieces")
	
	for item in pieces:
		#print(item)
		pieces[item].draw()
	
	
func _find_clusters(game_pieces = pieces):
	Logger.trace("[GameBoard] _find_clusters")

	var clusters = []
	
	#	find horizontal clusters
	for row in range(rows):
		
		var match_length = 1
		var match_type = -1
		
		for col in range(columns):
			
			var check_cluster = false
			
			if col == columns - 1:
				check_cluster = true
			else:
				# game pieces match and are greater than 0 which is the bomb type
				if game_pieces[Vector2(col, row)].type == game_pieces[Vector2(col + 1, row)].type && game_pieces[Vector2(col, row)].type >= 0:
					match_length += 1
					match_type = game_pieces[Vector2(col, row)].type
				else:
					check_cluster = true
					
			if check_cluster:
				if match_length >= 3:
					clusters.append( { "board_location": Vector2(col + 1 - match_length, row), "length": match_length, "horizontal": true, "type": match_type })
			
				match_length = 1	
					
				
	#	find vertical clusters
	
	for col in range(columns):
		
		var match_length = 1
		var match_type = -1
		
		for row in range(rows):
			var check_cluster = false
			if row == rows - 1:
				check_cluster = true
			else:
				# game pieces match and are greater than 0 which is the bomb type
				if game_pieces[Vector2(col, row)].type == game_pieces[Vector2(col, row + 1)].type && game_pieces[Vector2(col, row)].type >= 0:
					match_length += 1
					match_type = game_pieces[Vector2(col, row)].type
				else:
					check_cluster = true
					
			if check_cluster:
				if match_length >= 3:
					clusters.append( { "board_location": Vector2(col, row + 1 - match_length), "length": match_length, "horizontal": false, "type": match_type })
			
				match_length = 1	
	
	return clusters


#	return list of pieces to shift down
func _find_shifts():
	Logger.trace("[GameBoard] _find_shifts")

	var shifts = Array()
	
	for x in range(columns):
		for y in range(rows - 2, -1, -1):
			var key = Vector2(x, y)
			var key_below = Vector2(x, y + 1)
			var piece = pieces[key]
			var piece_below = pieces[key_below]
			if piece.type != -1:
				if piece_below.type == -1:
					shifts.append({ "from": key, "to": key_below })
	
	return shifts
	

func _remove_cluster(cluster):
	Logger.trace("[GameBoard] _remove_cluster")
#	print(cluster)
	
	var _pieces = []
	var pos = cluster.board_location
	var x_offset = 0
	var y_offset = 0
	
	for l in range(cluster.length):
		var key = Vector2(pos.x + x_offset, pos.y + y_offset)
		var piece = pieces[key]
		_pieces.append(piece)
		piece.type = -1
		
		if cluster.horizontal:
			x_offset += 1
		else:
			y_offset += 1	
		
	return _pieces
	
func _remove_clusters(clusters):
	Logger.trace("[GameBoard] _remove_clusters")
	
	var _pieces = []
	
	for cluster in clusters:
		_pieces.append_array(_remove_cluster(cluster))
		Game.emit_signal("resource_collected", cluster.type, cluster.length)
			
	if (_pieces.size() > 0):
		_destroy_hint()
					
	return _pieces
	
func _resolve_clusters():
	Logger.trace("[GameBoard] _resolve_clusters")
	
#	print(__dump__("type"))
	var clusters = _find_clusters()
#	print(clusters)
	
	while clusters.size() > 0:
		var _pieces = _remove_clusters(clusters)
#		print(__dump__("type"))
#		_shift_pieces()
		_resolve_pieces()
		clusters = find_clusters()
		
	return		

func _resolve_pieces():
	Logger.trace("[GameBoard] _resolve_pieces")
	print(resources)
	for piece in pieces.values():
		if piece.type == -1:
			var _rand = floor(rand_range(0, resources.size()))
			piece.type = resources[_rand]
			piece.is_selected = false
			piece.is_dirty = false
			piece.is_moving = false
			piece.draw()
			
		
func _shift_pieces():
	Logger.trace("[GameBoard] _shift_pieces")

	var shifts = _find_shifts()
	
	while shifts.size() > 0:
		
		for shift in shifts:
			print("from:%s, to:%s" % [shift.from, shift.to])
			_swap(pieces[shift.from], pieces[shift.to])
	
		shifts = _find_shifts()
		
			
func _swap(from_piece, to_piece, game_pieces = pieces ):
	
	Logger.trace("[GameBoard] _swap")
		
	var _f_loc = from_piece.loc
	var _t_loc = to_piece.loc
	var _f_pos = from_piece.pos 
	var _t_pos = to_piece.pos

	game_pieces[_f_loc] = to_piece
	game_pieces[_f_loc].loc = _f_loc
	game_pieces[_f_loc].pos = _f_pos
		
	game_pieces[_t_loc] = from_piece
	game_pieces[_t_loc].loc = _t_loc
	game_pieces[_t_loc].pos = _t_pos
	
	return
	
	
func _clear_and_store():
	Logger.trace("[GameBoard] _clear_and_store")
	var _holder = []
	for i in columns:
		for j in rows:
			if (pieces[Vector2(i, j)] != null):
				_holder.append(pieces[Vector2(i, j)])
				pieces[Vector2(i, j)] = null
				
	return _holder
	
	
func _shuffle():
	Logger.trace("[GameBoard] _shuffle")
	var _holder = _clear_and_store()
	for i in columns:
		for j in rows:
			var _rand = floor(rand_range(0, _holder.size()))
			var _piece = _holder[_rand] as GamePiece
			_piece.loc = Vector2(i, j)
			_piece.pos = _piece.loc * size
#			_piece.type = _swap.type 
#			_piece.pos = _swap.pos 
#			_piece.loc = _swap.loc 
			_piece.move_to(_piece.pos)
			pieces[_piece.loc] = _piece
			_holder.remove(_rand)
				
	if (is_deadlocked()):
		_shuffle()
	
		
func _init():
	Logger.trace("[GameBoard] _init")
	pass
	

func _ready():
	
	Logger.trace("[GameBoard] _ready")
	
	Game.game_board = self
	
	collect_bar1.value = 0
	collect_bar2.value = 0
	collect_bar3.value = 0
	collect_bar4.value = 0
	collect_icon1.visible = false
	collect_icon2.visible = false
	collect_icon3.visible = false
	collect_icon4.visible = false
	collect_label1.text = ""
	collect_label2.text = ""
	collect_label3.text = ""
	collect_label4.text = ""
	heat_bar.value = 0
	heat_icon.visible = false
	heat_label.text = ""
	message_text.visible = false
	
	

		
func _process(delta):
	
	# update ui on board
	collect_label1.text = "%s/%s" % [ collect_bar1.value, collect_bar1.max_value]
	collect_label2.text = "%s/%s" % [ collect_bar2.value, collect_bar2.max_value]
	collect_label3.text = "%s/%s" % [ collect_bar3.value, collect_bar3.max_value]
	collect_label4.text = "%s/%s" % [ collect_bar4.value, collect_bar4.max_value]
#	heat_label.text = "%s/%s" % [ heat_bar.value, heat_bar.max_value]

func __dump__(report):

	var r = ""
	
	r += "dump %s\n" % report
	
	
	for y in range(rows):
		for x in range(columns):
			var v = Vector2(x, y)
			var p = pieces[v]
			var s = ""
			match report:
				"type":
					s = "(%3s)" % [ p.type ]
				"dirty":
					s = "(%5s)" % [ p.is_dirty ]
				"position":
					s = "(%5s, %5s)" % [ p.pos.x, p.pos.y ]
				"location":
					s = "(%5s, %5s)" % [ p.loc.x, p.loc.y ]
			r += s
		r += "\n"
		
	r += "\n"
	
	return r


func _generate_hint():
	var _hints = find_all_matches()
	print(_hints)
		
	if (_hints):
		if (_hints.size() > 0):
			_destroy_hint()
			var _rand = floor(rand_range(0, _hints.size()))
			hint = hint_piece.instance()
			add_child(hint)
			hint.global_position = _hints[_rand].global_position
			hint.Setup(_hints[_rand].get_node("Frame/Icon").texture)


func _destroy_hint():
	if (hint):
		hint.queue_free()
		hint = null		


func _on_HintTimer_timeout():
	_generate_hint()
