extends Node2D

func start(message: String, color = Color.green, pos = Vector2.ZERO):
	$Label.text = "%s" % message
	$Label.add_color_override("font_color", color)
	global_position = pos
	$Timer.start(2)
	$UpTween.interpolate_property(self, "position", position, position + (Vector2(0, -1) * 50), 1.7, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	$ModulateTween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0), 1.9, Tween.TRANS_SINE, Tween.EASE_IN)
	$UpTween.start()
	$ModulateTween.start()	


func _on_Timer_timeout():
	queue_free()
