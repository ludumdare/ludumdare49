extends Node2D

export var show_collected : PackedScene

onready var game_board = $GameBoard
onready var waiter = $Waiter
onready var dialog = $Dialog

onready var waiter_yes = load("res://game/assets/waiter_yes.png")
onready var waiter_no = load("res://game/assets/waiter_yes.png")

var waiter_pos
var waiter_hpos

var dialog_pos
var dialog_hpos

var resources = []
var resource_amounts = []

func _init():
	Logger.trace("[GameMatch] _init")
	pass
	
	
func _ready():
	
	Logger.trace("[GameMatch] _ready")
	
	Game.game_board_completed = false
	Game.game_over = false
	
	waiter_pos = waiter.global_position
	dialog_pos = dialog.global_position
	
	waiter_hpos = waiter_pos + Vector2(-1,0) * 600
	dialog_hpos = dialog_pos + Vector2(-1,0) * 600
	
	_hide(0)
		
	
	# when a current block is detected we
	# load the details into the ts7
	if (Game.current_block >=0):
		
		var _block = Game.game_blocks[Game.current_block]
		resources = _block.resources
		resource_amounts = _block.resource_amounts
		
	else:
		
		resources = [ 0, 1, 2, 3, 4 ]	
		resource_amounts = [3, 3, 3, 3]
		print(resources)
		
	game_board.collect_bar1.max_value = resource_amounts[0]
	game_board.collect_bar2.max_value = resource_amounts[1]
	game_board.collect_bar3.max_value = resource_amounts[2]
	game_board.collect_bar4.max_value = resource_amounts[3]
	game_board.collect_bar1.value = 0
	game_board.collect_bar2.value = 0
	game_board.collect_bar3.value = 0
	game_board.collect_bar4.value = 0
	game_board.collect_icon1.texture = Game.get_icon_texture(resources[1])
	game_board.collect_icon2.texture = Game.get_icon_texture(resources[2])
	game_board.collect_icon3.texture = Game.get_icon_texture(resources[3])
	game_board.collect_icon4.texture = Game.get_icon_texture(resources[4])
	game_board.collect_label1.text = ""
	game_board.collect_label2.text = ""
	game_board.collect_label3.text = ""
	game_board.collect_label4.text = ""
	game_board.heat_icon.texture = Game.get_icon_texture(resources[0])
	game_board.heat_label.text = ""
	game_board.heat_bar.value = 0
	game_board.heat_bar.max_value = 20
	
	game_board.collect_icon1.visible = true
	game_board.collect_icon2.visible = true
	game_board.collect_icon3.visible = true
	game_board.collect_icon4.visible = true
	
	game_board.heat_icon.visible = true
		
	Game.game_board.resources = resources
	Game.game_board.columns = 7
	Game.game_board.rows = 6
	Game.game_board.create()
	Game.game_board.draw()
	
	Game.connect("resource_collected", self, "_on_resource_collected")
	
	Game.timer.start()


func _on_resource_collected(type, count):
	Logger.trace("[GameMatch] _on_resource_collected")
	Game.sound_manager.play("collect")	
	Logger.info("collected type:%s count:%s" % [type, count])

	var _pos = 0
	
	for _type in resources:
		
		if (type == _type):
			
			match _pos:
				
				0: 
					game_board.heat_bar.value += count		
					_show_collected(count, Color.red, Game.game_board.heat_icon.global_position)

				1: 
					game_board.collect_bar1.value += count
					_show_collected(count, Color.green, Game.game_board.collect_icon1.global_position)
					
				2: 
					game_board.collect_bar2.value += count
					_show_collected(count, Color.green, Game.game_board.collect_icon2.global_position)
					
				3: 
					game_board.collect_bar3.value += count
					_show_collected(count, Color.green, Game.game_board.collect_icon3.global_position)
					
				4: 
					game_board.collect_bar4.value += count
					_show_collected(count, Color.green, Game.game_board.collect_icon4.global_position)
				
		_pos += 1
		
	# increase smoke
	game_board.smoke_effect.start(game_board.heat_bar.value)
	
	# check for end game scenarios
	
	if (game_board.heat_bar.value >= game_board.heat_bar.max_value):
		Game.game_over_type = "chili"
		Game.emit_signal("game_over")

	# check for board completed
	
	var _completed = true
	
	if (game_board.collect_bar1.value < game_board.collect_bar1.max_value):
		_completed = false
		
	if (game_board.collect_bar2.value < game_board.collect_bar2.max_value):
		_completed = false
		
	if (game_board.collect_bar3.value < game_board.collect_bar3.max_value):
		_completed = false

	if (game_board.collect_bar4.value < game_board.collect_bar4.max_value):
		_completed = false

	if (_completed):
		_show()
		Game.game_board_completed = true
		Game.emit_signal("board_completed")
		
	
func _show_collected(count, color, pos):
	var _coll = show_collected.instance()
	add_child(_coll)
	var _msg = "%s" % count
	if count > 0:
		_msg = "+%s" % count
		
	_coll.start(_msg, color, pos)
			

func _on_CooldownTimer_timeout():

	if (game_board.heat_bar.value > 0):
		_show_collected(-1, Color.skyblue, Game.game_board.heat_icon.global_position)
		
	game_board.heat_bar.value -= 1
	
	if (game_board.heat_bar.value <0):
		game_board.heat_bar.value = 0
		

func _hide(speed = 1.0):
	
	var _t1 = Tween.new()
	add_child(_t1)
	_t1.interpolate_property(waiter, "position", waiter.position, waiter_hpos, speed, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	_t1.start()

	var _t2 = Tween.new()
	add_child(_t2)
	_t2.interpolate_property(dialog, "position", dialog.position, dialog_hpos, speed, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR)
	_t2.start()
	
	
func _show(speed = 1.0):
	
	var _t1 = Tween.new()
	add_child(_t1)
	_t1.interpolate_property(waiter, "position", waiter.position, waiter_pos, speed, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	_t1.start()
	
	var _t2 = Tween.new()
	add_child(_t2)
	_t2.interpolate_property(dialog, "position", dialog.position, dialog_pos, speed, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	_t2.start()
	
