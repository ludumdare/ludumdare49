extends Node2D

export (Array, Resource) var sounds


func play(name):
	
	var _player = AudioStreamPlayer.new() as AudioStreamPlayer
	add_child(_player)
	
	var _stream = _find_stream(name) as AudioStreamSample

	if (_stream):
		
		_player.stream = _stream
		_player.play()

		yield(_player, "finished")
		_player.queue_free()


func _find_stream(name):
	
	for _sound in sounds:
		if (_sound.name == name):
			return _sound.stream
			
