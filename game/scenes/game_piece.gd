class_name GamePiece
extends Area2D

# emitted when a piece is clicked
signal click_piece(piece)

onready var match_tween = $MatchTween


# unique identifier
var uuid : UUID = UUID.new()

# type of piece
var type : int = 0

# loication on game board
var loc : Vector2 = Vector2.ZERO

# position on game scene
var pos : Vector2 = Vector2.ZERO

# has this piece been selected?
var is_selected : bool = false

# piece needs updating?
var is_dirty : bool = false

# is this piece moving?
var is_moving : bool = false


var timer = 0;
var frequency = 0.09;
var amplitude = 0.09;


var textures = [
	"res://game/assets/piece_0.png",
	"res://game/assets/piece_1.png",
	"res://game/assets/piece_2.png",
	"res://game/assets/piece_3.png",
	"res://game/assets/piece_4.png",
	"res://game/assets/piece_5.png",
	"res://game/assets/piece_6.png",
	"res://game/assets/piece_7.png",
	"res://game/assets/piece_8.png",
	"res://game/assets/piece_9.png",
	"res://game/assets/piece_a.png",
]

enum types { 
	TYPE_WARP, TYPE_SPEED, TYPE_SHIELD, TYPE_WEAPON, TYPE_POWER }


var frame = "res://game/assets/piece_frame.png"
var selected = "res://game/assets/piece_selected.png"


func set_type():
	_set_type()


func move_to(pos):
	
	var _old_pos = position
	var _new_pos = pos
	
	# tween to new position
	
	var _t1 = Tween.new()
	add_child(_t1)
	_t1.interpolate_property(self, "position", _old_pos, _new_pos, 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	_t1.start()
	
func hide():
	var _t1 = Tween.new()
	add_child(_t1)
	_t1.interpolate_property($Frame, "scale", Vector2(1,1), Vector2(0,0), 0.5, Tween.TRANS_SINE, Tween.TRANS_LINEAR)
	_t1.start()
	yield(_t1, "tween_completed")
	visible = false
	

func draw():
	
	position = pos
	$Frame.scale = Vector2.ONE
	
#	if (type == -1):
#		visible = false
#		return
		
	var texture = load(textures[type])
	$Frame/Icon.texture = texture
	
	var _frame = frame
	if (is_selected):
		_frame = selected
		
#	texture = load(_frame)
#	$Frame.texture = texture
	visible = true
	
	
func _process(delta):
	
	if (is_selected):
		$Frame.scale.x = 1 + cos(timer*frequency)*amplitude
		$Frame.scale.y = 1 + sin(timer*frequency)*amplitude
		timer += 1

func _init():
	_set_type()


func _set_type():
	var index = floor(rand_range(0, textures.size()))
	type = index



func _on_GamePiece_input_event(viewport, event, shape_idx):
	
	# if we are already resolving then
	# prevent player from selecting
	# another piece
	
	if (Game.is_resolving):
		return
	
	# check for clicked piece and reset scale
		
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.is_pressed():
				Game.emit_signal(Game.SIGNAL_PIECE_CLICKED, self)
				$Frame.scale = Vector2(1, 1)


func __dump__():
	return "n:(%s) t:(%s) l:%s p:%s s:%s d:%s m:%s" % [ name, type, loc, pos, is_selected, is_dirty, is_moving]
