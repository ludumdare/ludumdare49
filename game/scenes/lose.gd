extends Node2D

onready var baloon_text = $Dialog/Baloon/Text

func _ready():
	print(Game.game_over_type)
	
	if Game.game_over_type == "timer":
		baloon_text.text = "times up!\nmaybe next time, hero!"

	if Game.game_over_type == "chili":
		baloon_text.text = "hot stuff!too many chilis!"


func _on_PlayButton_pressed():
	Core.change_state(Ludum.GAME_STATE_PLAY)


func _on_HomeButton_pressed():
	Core.change_state(Ludum.GAME_STATE_HOME)
