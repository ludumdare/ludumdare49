extends Node2D

export var move_delay = 1.0
export var window_manager : Resource

signal piece_clicked(piece)
signal board_resolved()
signal resource_collected(resource, amount)
signal city_block_clicked(city_block)
signal game_over()
signal board_completed()

const SIGNAL_PIECE_CLICKED = "piece_clicked"
const SIGNAL_BOARD_RESOLVED = "board_resolved"


var show_tutorial : bool = true
var game_blocks = []
var heat = 0
var current_block : int = -1

var selected : bool
var game_board : GameBoard
var is_resolving : bool = false
var game_board_completed = false
var game_over = false
var game_over_type = ""
var game_round = 1

onready var music_manager = $MusicManager
onready var sound_manager = $SoundManager
onready var timer = $Timer

var textures = [
	"res://game/assets/piece_0.png",
	"res://game/assets/piece_1.png",
	"res://game/assets/piece_2.png",
	"res://game/assets/piece_3.png",
	"res://game/assets/piece_4.png",
	"res://game/assets/piece_5.png",
	"res://game/assets/piece_6.png",
	"res://game/assets/piece_7.png",
	"res://game/assets/piece_8.png",
	"res://game/assets/piece_9.png",
	"res://game/assets/piece_a.png",
]



func _ready():
	
	Logger.set_logger_level(Logger.LOG_LEVEL_INFO)
	
	connect("piece_clicked", self, "_on_piece_clicked")
	connect("resource_collected", self, "_on_resource_collected")
	connect("game_over", self, "_on_game_over")
	connect("board_completed", self, "_on_board_completed")
	Core.connect("state_changed", self, "_on_state_changed")
	
	if (window_manager):
		WindowManager.load(window_manager)
	
	Ludum.start()
	Core.change_state(Ludum.GAME_STATE_PLAY)
	
	music_manager.play("Stage1")
	
	
func _init():
	pass
	
	
func setup():
	
	# prepare the cities
	
	game_blocks = []
	
	var _low = 5
	var _high = 18
#	var _low = 5
#	var _high = 22
	
	for i in range(32):
		
		var _resources = [1, 2, 3, 4, 5, 6, 7, 8, 9]
		
		var _block = {}
		_block.id = i
		_block.icon = floor(rand_range(0,3))
		_block.resources = [0]
		var _r
		_r = floor(rand_range(0,_resources.size()))
		_block.resources.append(_resources[_r])
		_resources.remove(_r)
		_r = floor(rand_range(0,_resources.size()))
		_block.resources.append(_resources[_r])
		_resources.remove(_r)
		_r = floor(rand_range(0,_resources.size()))
		_block.resources.append(_resources[_r])
		_resources.remove(_r)
		_r = floor(rand_range(0,_resources.size()))
		_block.resources.append(_resources[_r])
		_resources.remove(_r)
		_block.resource_amounts = []
		_block.resource_amounts.append(floor(rand_range(_low, _high)))
		_block.resource_amounts.append(floor(rand_range(_low, _high)))
		_block.resource_amounts.append(floor(rand_range(_low, _high)))
		_block.resource_amounts.append(floor(rand_range(_low, _high)))
		_block.is_open = true
		_block.is_completed = false
		_block.is_alerted = false
		game_blocks.append(_block)
		
		timer.stop()
		timer.wait_time = 300
		timer.start()
		timer.paused = true
		
#	game_blocks[0].is_open = true
#	game_blocks[0].is_completed = false
#
#	game_blocks[1].is_open = true
#	game_blocks[1].is_completed = false
		

func load_board(game_block):
	pass



func _on_piece_clicked(piece : GamePiece):
	
	# nothing was selected, lets make the
	# piece we clicked the selected piece
	
	if (!selected):
		game_board.selected = piece
		selected = true
		piece.is_selected = true
		sound_manager.play("pick")
		return
	
	# if we clicked the same piece, then
	# reset them both
		
	if (piece.uuid == game_board.selected.uuid):
		selected = false
		piece.is_selected = false
		sound_manager.play("unpick")
		return
		
	# if a diff piece was selected
	# we see if we can swap them.
	#
	# if we can, great, if not then
	# reset the selected piece
	
	var _from = game_board.selected
	var _to = piece
	
	var _from_loc = _from.loc 
	var _to_loc = _to.loc
	
#	print(_from_loc)
#	print(_to_loc)

	if (!game_board.can_swap(_to)):
		_from.is_selected = false
		selected = false
		sound_manager.play("unpick")
		return
	

	is_resolving = true
	
	# tween from piece to new position
	var _t1 = Tween.new()
	add_child(_t1)
	_t1.interpolate_property(_from, "position", _from.pos, _to.pos, move_delay, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	_t1.start()
	
	# tween to piece to new position and
	# wait for completion
	
	var _t2 = Tween.new()
	add_child(_t2)
	_t2.interpolate_property(_to, "position", _to.pos, _from.pos, move_delay, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	_t2.start()

	yield(_t2, "tween_completed")
	_t1.queue_free()
	_t2.queue_free()
	print("done")
	
	game_board.swap(_from, _to)
		
	var _clusters = game_board.find_clusters()
		
	if (_clusters.size() > 0):

		while (_clusters != null && _clusters.size() > 0):		
			
			game_board.selected.is_selected = false		
			game_board.selected.draw()
			yield(get_tree(), "idle_frame")
			
			yield(_resolve_board(_clusters), "completed")
			game_board.resolve_pieces()
			_clusters = game_board.find_clusters()
		
			if (game_board.is_deadlocked()):
				print("board is deadlocked")
				game_board.shuffle()
				_clusters = game_board._resolve_clusters()
				
			yield(get_tree(), "idle_frame")
			yield(get_tree().create_timer(0.5), "timeout")
			
		selected = false
				
#		_from.is_selected = false
#		_from.draw()
#
#		_to.is_selected = false
#		_to.draw()
		
	else:
		
		_from = game_board.pieces[_from_loc]
		_to = game_board.pieces[_to_loc]
		
#		print(_from)
#		print(_to)
		
		# tween from piece to new position
		_t1 = Tween.new()
		add_child(_t1)
		_t1.interpolate_property(_from, "position", _from.pos, _to.pos, move_delay, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		_t1.start()
		
		# tween to piece to new position and
		# wait for completion
		
		_t2 = Tween.new()
		add_child(_t2)
		_t2.interpolate_property(_to, "position", _to.pos, _from.pos, move_delay, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
		_t2.start()

		yield(_t2, "tween_completed")
		_t1.queue_free()
		_t2.queue_free()
		print("done")
		
		game_board.swap(_from, _to)
		
	game_board.draw()
	is_resolving = false
	
	if game_board_completed:
		_end_board()
		
	if game_over:
		_game_over()
	
	
func _resolve_board(clusters):
	
	# get a list of all pieces to be
	# removed from the board
	
	var _pieces = game_board.remove_clusters(clusters)
#	print(_pieces)
	
	if (_pieces):
		for _piece in _pieces:
			_piece.type = -1
			_piece.is_selected = false
			_piece.hide()
			yield(get_tree(), "idle_frame")
		
	yield(get_tree().create_timer(move_delay * 1.5), "timeout")
	yield(get_tree(), "idle_frame")
	
	var _shifts = game_board.find_shifts()
#
	while _shifts.size() > 0:
#
		for _shift in _shifts:

			var _from = game_board.get_piece(_shift.from)
			var _to = game_board.get_piece(_shift.to)
			_from.move_to(_to.pos)
			game_board.swap(_from, _to)
#
		_shifts = game_board.find_shifts()

	yield(get_tree(), "idle_frame")
		

func _add_resource(type, count):

	Game.emit_signal("resource_collected", type, count)
	
	if (game_board.resources.has(type)):
		game_board.resources[type] += count
	else:
		game_board.resources[type] = count


func get_icon_texture(id):
	var _texture = load(textures[id])
	return _texture
	

func _on_resource_collected(type, amount):
	print(type, amount)


func _on_game_over():
	print("game over man")
	game_over = true
	timer.stop()
	
func _on_board_completed():
	print("board solved")
	
func _game_over():
	SceneManager.transition_to("res://game/scenes/lose.tscn")
	
func _end_board():
	
	if current_block >= 0:
		var _block = game_blocks[current_block]
		_block.is_completed = true
		_block.is_alerted = false
		_block.is_open = false
		
		
	for _block in game_blocks:
		if !_block.is_completed:
			SceneManager.transition_to("res://game/scenes/game_city.tscn")
			return

	SceneManager.transition_to("res://game/scenes/win.tscn")
			
	
			
	

func _on_state_changed(state):
	
	match state:
		
		Ludum.GAME_STATE_PLAY:
			
			setup()



func _on_Timer_timeout():
	game_over_type = "timer"
	_game_over()
